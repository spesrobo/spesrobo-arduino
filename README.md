# SpesRobo Arduino software

## Getting started
- Install Arduino IDE
    - [Download here for Windows](https://www.arduino.cc/download_handler.php?f=/arduino-1.6.7-windows.exe) or for Debian based OS run ```sudo apt-get install arduino```
- Copy this repo ```git clone https://LukicDarkoo@bitbucket.org/spesrobo/spesrobo-arduino.git```
- Copy content from folder ```/libraries``` (this repo) to ```arduino_folder/libraries```
- Open Arduino IDE and import the project
    - Change ```Tools > Serial Port```
    - Change ```Tools > Board``` to Arduino Mega 2560

## Serial commands
Serial communication is used to communicate between host computer and Arduino platform by sending ASCII characters like ```af50```. Arduino IDE has built in Serial Monitor which is great tool for testing Arduino applications.

### Getting started with Serial Monitor
- Run Arduino IDE
- Open Serial Monitor by clicking on green loupe on right side (labeled as Serial Monitor)
- In new Window Form set bound rate at 115200
- Type commands in the input text field

### Commands
- Robotic Arm
    - Set angle of first crank ```af50``` where ```50``` is the angle
    - Set angle of second crank ```as50``` where ```50``` is the angle
    - Set angle of third crank ```af50``` where ```50``` is the angle
    - Set arm coordinates ```ax150ay150az150``` where numbers are values in millimeters
- Check switch...case in HarvestDrone.ino for more...