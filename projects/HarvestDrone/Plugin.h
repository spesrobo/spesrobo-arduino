#ifndef Plugin_h
#define Plugin_h

#define PLUGIN_READ_PIN 22
#define PLUGIN_WRITE_PIN 23
 
class Plugin {
  public:
    static void init();
    static void control();
    static void update();

  private:
    static unsigned char lastState;
    static int currentSampleTicks;
    static int previousSampleTicks;
    static int lastSampleTime;
};

unsigned char Plugin::lastState = 0;
int Plugin::currentSampleTicks = 0;
int Plugin::previousSampleTicks = 0;
int Plugin::lastSampleTime = 0;
 
void Plugin::init() {
  Wire.begin();
}

void Plugin::update() {
    if (digitalRead(PLUGIN_READ_PIN) != lastState) {
      lastState = digitalRead(PLUGIN_READ_PIN);
      currentSampleTicks++;
    }

    if (millis() > lastSampleTime + 1000) {
      lastSampleTime = millis();
      previousSampleTicks = currentSampleTicks;
      currentSampleTicks = 0;
    }
}
 
void Plugin::control() {
  delay(SERIAL_DELAY);
  
  switch(Serial.read()) {
    case 'r':
      Serial.print("Plugin::MD::");
      Serial.println(Plugin::previousSampleTicks);
      break;

    case 'h':
      digitalWrite(PLUGIN_WRITE_PIN, HIGH);
      break;

    case 'l':
      digitalWrite(PLUGIN_WRITE_PIN, LOW);
      break;
  }
  
  /*
  Wire.requestFrom(8, 16);

  Serial.print("Plugin::");
  while (Wire.available()) {
    unsigned char c = (unsigned char)Wire.read();
    if (c != 255) {
      Serial.print((char)c);
    }
  }
  Serial.println();
  */
}
 
#endif
