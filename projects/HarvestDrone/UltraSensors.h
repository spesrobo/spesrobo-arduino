#ifndef UltraSensors_h
#define UltraSensors_h

class UltraSensors {
  public:
    // Trigger pin, Echo pin, Max Distance
    static NewPing coreFront;
    static NewPing coreLeft;
    static NewPing coreRight;
    
    static void update() {
      /*
      delay(50);
      int uS = coreFront.ping();
      Serial.print("Ping: ");
      Serial.print(uS / US_ROUNDTRIP_CM);
      Serial.println("cm");
      */
    };
    
};

NewPing UltraSensors::coreRight(51, 41, 200);
NewPing UltraSensors::coreLeft(30, 31, 200);
NewPing UltraSensors::coreFront(49, 33, 200);

#endif
