#define SERIAL_DELAY 4
 
// Incudes Tester class
#define DEBUG 1
 
#include <RobotArm.h>
#include <NewPing.h>
#include <Wire.h>
 
#include "Arm.h"
#include "Move.h"
#include "SpeedSensor.h"
#include "UltraSensors.h"
#include "Kinect.h"
#include "Bunker.h"
#include "Plugin.h"
 
#if DEBUG == 1
  #include "Tester.h"
#endif
 
 
void setup() {
  Serial.begin(115200);
  Serial.setTimeout(SERIAL_DELAY);
 
  Move::init();
  Arm::init();
  SpeedSensor::init();
  Kinect::init();
  Bunker::init();
  Plugin::init();
}
 
void loop() {
  UltraSensors::update();
  Arm::update();
  SpeedSensor::update();
  Move::update();
  //Plugin::update();
 
  if (Serial.available() > 0) {
    switch(Serial.read()) {
      case 'a':
        Arm::control();
      break;
     
      case 'm':
        Move::control();
      break;
 
      case 'k':
        Kinect::control();
      break;
 
      case 'p':
        Plugin::control();
      break;
 
      case 'b':
        Bunker::control();
        break;
 
      #if DEBUG == 1
        case 't':
          Tester::control();
        break;
      #endif
    }
  }
}
