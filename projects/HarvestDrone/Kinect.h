#ifndef Kinect_h
#define Kinect_h

#define KINECT_SERVO_PIN 8

class Kinect {
  public:
    static void update();
    static void init();
    static void control();

   private:
    static Servo servo;
};

Servo Kinect::servo;

void Kinect::init() {
  Kinect::servo.attach(KINECT_SERVO_PIN);
}

void Kinect::control() {
  delay(SERIAL_DELAY);
  
  switch(Serial.read()) {   
      case 'a':
        int angle = Serial.parseInt();
        if (angle < -45 || angle > 45) {
          return; 
        }

        int normalizedAngle = angle + 80;
      
        Kinect::servo.write(normalizedAngle);
        break;
  }
}

#endif
