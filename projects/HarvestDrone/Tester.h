#ifndef Tester_h
#define Tester_h

class Tester {
  public:
    static void control();

  private:
    static int selectedPin;
};

int Tester::selectedPin = 0;

// tp28td1 tp9ta20

void Tester::control() {
  delay(SERIAL_DELAY);
  
  switch (Serial.read()) {
    case 'p':
      selectedPin = Serial.parseInt();
    break;

    case 'd':
      digitalWrite(selectedPin, Serial.parseInt() == 0 ? LOW : HIGH);
    break;

    case 'a':
      analogWrite(selectedPin, Serial.parseInt());
    break;
  }
}

#endif
