#ifndef SpeedSensor_h
#define SpeedSensor_h

#define SPEED_SENSOR_LEFT A0
#define SPEED_SENSOR_RIGHT A1

class SpeedSensor {
  public:
    static void init();
    static void update();
    
    static long int getLeftDistance() { return SpeedSensor::leftTacts; }
    static long int getRightDistance() { return SpeedSensor::rightTacts; }
    
  private:
    static boolean leftLastStatus;
    static boolean rightLastStatus;
    
    static long int leftTacts;
    static long int rightTacts;
};

boolean SpeedSensor::leftLastStatus = false;
boolean SpeedSensor::rightLastStatus = false;

long int SpeedSensor::leftTacts = 0;
long int SpeedSensor::rightTacts = 0;

void SpeedSensor::init() {
  pinMode(SPEED_SENSOR_LEFT, INPUT);
  pinMode(SPEED_SENSOR_RIGHT, INPUT);
}

void SpeedSensor::update() {
  if (SpeedSensor::leftLastStatus != digitalRead(SPEED_SENSOR_LEFT)) {
    SpeedSensor::leftLastStatus = !SpeedSensor::leftLastStatus;
    SpeedSensor::leftTacts++;
    

    
  }
  
  if (SpeedSensor::rightLastStatus != digitalRead(SPEED_SENSOR_RIGHT)) {
    SpeedSensor::rightLastStatus = !SpeedSensor::rightLastStatus;
    SpeedSensor::rightTacts++;
    
    Serial.print("{\"TactsRight\": \"");
    Serial.print(SpeedSensor::rightTacts);
    Serial.println("\"}");
  }
}

#endif
