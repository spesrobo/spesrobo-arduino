#include <PID_v1.h>
#include <RobotArm.h>
#include <Servo.h>

#ifndef Arm_h
#define Arm_h

// ax10ay182az163

#define ARM_PIN_X_MOTOR_SPEED 9
#define ARM_PIN_X_MOTOR_DIRECTION 39
#define ARM_PIN_X_SENSOR 2
#define ARM_DIR_LEFT 1
#define ARM_DIR_RIGHT 2
#define ARM_DIR_STOP 3
#define ARM_RIGHT_SWITCH_PIN 22
#define ARM_LEFT_SWITCH_PIN 25


// Veoma bitno, treba promijeniti kada se zategne kajis
#define ARM_X_DEFAULT_SPEED 45  // Maksimalna brzina
#define ARM_X_TOL 2             // Tolerancija, maksimlano odstupanje od zadate vrijednosti X
#define ARM_RANGE 250           // Opseg po kome se ruka krece je od -ARM_RANGE do ARM_RANGE

class Arm {
  public:
    static float x; 
    static float y;
    static float z;
    
    static void init();
    static void control();
    static void update();
    
  private:
    static RobotArm core;
    static float currentX;
    static unsigned char directionX;
    
    static void stepChanged();
};

RobotArm Arm::core;
float Arm::x = 0;
float Arm::y = 0;
float Arm::z = 0;
float Arm::currentX = 0;
unsigned char Arm::directionX = ARM_DIR_STOP;

void Arm::stepChanged() {
    if (Arm::directionX == ARM_DIR_LEFT) {
      Arm::currentX--;
    } else if (Arm::directionX == ARM_DIR_RIGHT) {
        Arm::currentX++;
    }

    Serial.println(Arm::currentX);
}

void Arm::update() {
  Arm::core.update();

  // Left side reached
  if (digitalRead(ARM_LEFT_SWITCH_PIN) == LOW) {
      Arm::x = -ARM_RANGE;
      Arm::currentX = -(ARM_RANGE + 10);
      Arm::directionX = ARM_DIR_RIGHT;
  }
  
  // Right side reached
  if (digitalRead(ARM_RIGHT_SWITCH_PIN) == LOW) {
      Arm::x = ARM_RANGE;
      Arm::currentX = ARM_RANGE + 10;
      Arm::directionX = ARM_DIR_LEFT;
  }

  // Go to X
  if (abs(Arm::currentX - Arm::x) > ARM_X_TOL) {
    if (Arm::currentX > Arm::x) {
        Arm::directionX = ARM_DIR_LEFT;
        analogWrite(ARM_PIN_X_MOTOR_SPEED, ARM_X_DEFAULT_SPEED);
        digitalWrite(ARM_PIN_X_MOTOR_DIRECTION, LOW);
    } else {
        Arm::directionX = ARM_DIR_RIGHT;
        analogWrite(ARM_PIN_X_MOTOR_SPEED, ARM_X_DEFAULT_SPEED);
        digitalWrite(ARM_PIN_X_MOTOR_DIRECTION, HIGH);
    }
  } else {
      // Power saver
      
      Arm::directionX == ARM_DIR_STOP;
      analogWrite(ARM_PIN_X_MOTOR_SPEED, 0);
      digitalWrite(ARM_PIN_X_MOTOR_DIRECTION, LOW);
  }
}


void Arm::init() {
    Arm::core.servoDirectionFixes[ROBOTARM_SECOND_CRANK] = true;
    Arm::core.servoDirectionFixes[ROBOTARM_FIRST_CRANK] = false;
    Arm::core.servoDirectionFixes[ROBOTARM_THIRD_CRANK] = false;
    Arm::core.servoAngleFixes[ROBOTARM_FIRST_CRANK] = -30;
    Arm::core.servoAngleFixes[ROBOTARM_SECOND_CRANK] = 50;
    Arm::core.servoAngleFixes[ROBOTARM_THIRD_CRANK] = -120;
    Arm::core.servoDistances[ROBOTARM_DISTANCE_FIRST_SECOND] = 182;
    Arm::core.servoDistances[ROBOTARM_DISTANCE_SECOND_THIRD] = 163;
    Arm::core.servosPins[ROBOTARM_SECOND_CRANK] = 12;
    Arm::core.servosPins[ROBOTARM_FIRST_CRANK] = 4;
    Arm::core.servosPins[ROBOTARM_THIRD_CRANK] = 11;
    Arm::core.servosPins[ROBOTARM_ROTATION_CRANK] = 0; // Disable
    Arm::core.servosPins[ROBOTARM_GRIPPER] = 10;
    Arm::core.defaultAngles[ROBOTARM_FIRST_CRANK] = 90;
    Arm::core.defaultAngles[ROBOTARM_THIRD_CRANK] = 180;
    Arm::core.defaultAngles[ROBOTARM_SECOND_CRANK] = 90;
    Arm::core.smoothMovements = true;
    Arm::core.keepCrankParalel = true;
    Arm::core.attach();

    // X motor setup
    pinMode(ARM_PIN_X_MOTOR_SPEED, OUTPUT);
    pinMode(ARM_PIN_X_MOTOR_DIRECTION, OUTPUT);
    pinMode(ARM_PIN_X_SENSOR, INPUT);
    pinMode(ARM_LEFT_SWITCH_PIN, INPUT);
    pinMode(ARM_RIGHT_SWITCH_PIN, INPUT);

    // X tracker
    attachInterrupt(digitalPinToInterrupt(ARM_PIN_X_SENSOR), Arm::stepChanged, RISING);
}

void Arm::control() {
  delay(SERIAL_DELAY);
  
  switch(Serial.read()) {       
      case 'b':
        
        break;
    
      case 'f':
        Arm::core.keepCrankParalel = true;
        Arm::core.setAngle(ROBOTARM_FIRST_CRANK, Serial.parseInt());
        break;

      case 's':
        Arm::core.keepCrankParalel = true;
        Arm::core.setAngle(ROBOTARM_SECOND_CRANK, Serial.parseInt());
        break;
        
       case 't':
        Arm::core.keepCrankParalel = false;
        Arm::core.setAngle(ROBOTARM_THIRD_CRANK, Serial.parseInt());
        break;

      case 'g':  
        Arm::core.setAngle(ROBOTARM_GRIPPER, map(Serial.parseInt(), 0, 100, 57, 82));
        break;
       
      case 'x':
        Arm::x = Serial.parseFloat();
        break;
        
      case 'y':
        Arm::y = Serial.parseFloat();
        break;
        
      case 'z':
        Arm::z = Serial.parseFloat();
        Arm::core.setCoordinates(x, y, z);
        Arm::core.printAngles();
        break;
    }
}

#endif


