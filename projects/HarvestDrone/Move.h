#ifndef Move_h
#define Move_h

#define MOVE_REL_LEFT_PIN 27
#define MOVE_REL_RIGHT_PIN 31
#define MOVE_VOLTAGE_LEFT_PIN 3
#define MOVE_VOLTAGE_RIGHT_PIN 5
#define MOVE_ENCODER_LEFT_PIN 18
#define MOVE_ENCODER_RIGHT_PIN 19

#define MOVE_BACK HIGH
#define MOVE_FORWARD LOW
#define MOVE_STOP 2

#define MOVE_LEFT 1
#define MOVE_RIGHT 2

#define MOVE_TOL 5

#include <PID_v1.h>

class Move {
  public:
    static int voltageLeft;
    static int voltageRight;
  
    static void init();
    static void update();
    static void control();
    static void move(unsigned char, unsigned char);
    
   private:
     static void setDistance(int);
     static boolean stopAfterDistance;
     
     static double leftDistanceToPass;
     static double rightDistanceToPass;
     static double leftDistanceToPassPID;
     static double rightDistanceToPassPID;
     
    static void leftStepChanged();
    static void rightStepChanged();
    static double leftDistance;
    static double rightDistance;
    static char leftDirection;
    static char rightDirection;

    static boolean sendData;

    static PID *leftPID;
};

PID *Move::leftPID = NULL;
boolean Move::sendData = false;
int Move::voltageLeft = 0;
int Move::voltageRight = 0;
boolean Move::stopAfterDistance = false;
double Move::leftDistanceToPass = 0;
double Move::rightDistanceToPass = 0;
double Move::leftDistance = 0;
double Move::rightDistance = 0;
double Move::leftDistanceToPassPID = 0;
double Move::rightDistanceToPassPID = 0;
char Move::leftDirection = MOVE_STOP;
char Move::rightDirection = MOVE_STOP;

void Move::leftStepChanged() {
  if (Move::stopAfterDistance == true && Move::leftDirection != MOVE_STOP) {
    Move::leftDistance += (Move::leftDirection == MOVE_FORWARD) ? 1 : -1;
    Move::update();

    if (sendData == true) {
      Serial.print("Move::leftDistance::");  
      Serial.println(Move::leftDistance);
    }
  }
}

void Move::rightStepChanged() {
  if (Move::stopAfterDistance == true && Move::rightDirection != MOVE_STOP) {
    Move::rightDistance += (Move::rightDirection == MOVE_FORWARD) ? 1 : -1;
    Move::update();

    if (sendData == true) {
      Serial.print("Move::rightDistance::");  
      Serial.println(Move::rightDistance);
    }
  }
}


void Move::move(unsigned char wheel, unsigned char where) {
    switch (where) {
      case MOVE_FORWARD: case MOVE_BACK:
        if (wheel == MOVE_LEFT) {
          leftDirection = where;      
          digitalWrite(MOVE_REL_LEFT_PIN, where); 
          analogWrite(MOVE_VOLTAGE_LEFT_PIN, voltageLeft);
        } else {
          rightDirection = where;      
          digitalWrite(MOVE_REL_RIGHT_PIN, where);     
          analogWrite(MOVE_VOLTAGE_RIGHT_PIN, voltageRight); 
        }
        break;
        
      case MOVE_STOP:
        if (wheel == MOVE_LEFT) {
          leftDirection = MOVE_STOP;
          digitalWrite(MOVE_REL_LEFT_PIN, LOW);
          analogWrite(MOVE_VOLTAGE_LEFT_PIN, 0);
        } else {
          rightDirection = MOVE_STOP;
          digitalWrite(MOVE_REL_RIGHT_PIN, LOW);
          analogWrite(MOVE_VOLTAGE_RIGHT_PIN, 0);
        }
        break;
    }
}

void Move::init() {
    // Init relays
    pinMode(MOVE_REL_LEFT_PIN, OUTPUT);
    pinMode(MOVE_REL_RIGHT_PIN, OUTPUT);

    // Init voltage
    pinMode(MOVE_VOLTAGE_LEFT_PIN, OUTPUT);
    pinMode(MOVE_VOLTAGE_RIGHT_PIN, OUTPUT);

    // Init encoders
    attachInterrupt(digitalPinToInterrupt(MOVE_ENCODER_LEFT_PIN), Move::leftStepChanged, RISING);
    attachInterrupt(digitalPinToInterrupt(MOVE_ENCODER_RIGHT_PIN), Move::rightStepChanged, RISING);

    // PID
    // leftPID = new PID(&(Move::leftDistance), &(Move::leftDistanceToPassPID), &(Move::leftDistanceToPass), 5.0, 0.0, 0.0, AUTOMATIC);
}



void Move::update() {
  if (Move::stopAfterDistance == true) {

    // Control left wheel
    if (abs(Move::leftDistanceToPass - Move::leftDistance) > MOVE_TOL) {
      if (Move::leftDistanceToPass > Move::leftDistance) {
          Move::move(MOVE_LEFT, MOVE_FORWARD);
      } else {
          Move::move(MOVE_LEFT, MOVE_BACK);
      }
    } else {
      Move::move(MOVE_LEFT, MOVE_STOP); 
    }

    // Control right wheel
    if (abs(Move::rightDistanceToPass - Move::rightDistance) > MOVE_TOL) {
      if (Move::rightDistanceToPass > Move::rightDistance) {
          Move::move(MOVE_RIGHT, MOVE_FORWARD);
      } else {
          Move::move(MOVE_RIGHT, MOVE_BACK);
      }
    } else {
      Move::move(MOVE_RIGHT, MOVE_STOP);
    }
  }
}

void Move::setDistance(int distance) {
  if (distance != 0) {
    stopAfterDistance = true;
    leftDistanceToPass += distance;
    rightDistanceToPass += distance;
  } else {
    stopAfterDistance = false;  
  }
}

void Move::control() {
  delay(SERIAL_DELAY);
  
  switch (Serial.read()) {
      // Forward
      case 'f':
        Move::move(MOVE_LEFT, MOVE_FORWARD);
        Move::move(MOVE_RIGHT, MOVE_FORWARD);  
        Move::setDistance(Serial.parseInt());
      break;
      
      // Back
      case 'b':
        Move::move(MOVE_LEFT, MOVE_BACK);
        Move::move(MOVE_RIGHT, MOVE_BACK);
        Move::setDistance(Serial.parseInt() * -1);
      break;
       
      // Left
      case 'l':
        Move::move(MOVE_LEFT, MOVE_BACK);
        Move::move(MOVE_RIGHT, MOVE_FORWARD);
      break;
      
      // Right
      case 'r':
        Move::move(MOVE_LEFT, MOVE_FORWARD);
        Move::move(MOVE_RIGHT, MOVE_BACK);
      break;

      // Send data
      case 'd':
        if (Serial.read() == 't') {
            Move::sendData = true;
        } else {
          Move::sendData = false;  
        }
        break;
      
      // Turn off everything
      case 'o':
        Move::move(MOVE_LEFT, MOVE_STOP);
        Move::move(MOVE_RIGHT, MOVE_STOP);
      break;
      
      // Set voltage
      case 'v':
        int voltage = Serial.parseInt();
      
        Move::voltageLeft = voltage;
        Move::voltageRight = voltage;

        Move::move(MOVE_LEFT, Move::leftDirection);
        Move::move(MOVE_RIGHT, Move::rightDirection);
      break;


    }
}

#endif
