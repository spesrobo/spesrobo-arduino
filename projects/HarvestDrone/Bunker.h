#ifndef Bunker_h
#define Bunker_h

#define BUNKER_PIN 6

class Bunker {
  public:
    static void init();
    static void prepare();
    static void standby();
    static void angle(int);
    static void control();

  private:
    static Servo servo;
};

Servo Bunker::servo;

void Bunker::init() {
  Bunker::servo.attach(BUNKER_PIN);
  Bunker::prepare();
}

void Bunker::prepare() {
  Bunker::servo.write(103);
}

void Bunker::standby() {
  Bunker::servo.write(180);  
}

void Bunker::angle(int angle) {
  int fixedAngle = map(angle, 0, 100, 103, 180);
  Bunker::servo.write(fixedAngle);
}

void Bunker::control() {
  delay(SERIAL_DELAY);
  
  switch(Serial.read()) {   
      case 'p':
        Bunker::prepare();
        break;

      case 's':
        Bunker::standby();
        break;

      case 'a':
        Bunker::angle(Serial.parseInt());
        break;
  }
}

#endif
